package com.com.mino.mybatistest.mapper;
import com.com.mino.mybatistest.entity.UserInfoEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface UserInfoMapper {
    @Select("SELECT email, uuid, reg_dt, upd_dt FROM userinfo WHERE email = #{email}")
    UserInfoEntity getUserInfoByEmail(String email);
}
