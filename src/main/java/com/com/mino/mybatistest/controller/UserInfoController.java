package com.com.mino.mybatistest.controller;

import com.com.mino.mybatistest.dto.UserInfoDto;
import com.com.mino.mybatistest.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UserInfoController {

    @Autowired
    UserInfoService userInfoService;


    @GetMapping("/{email}")
    public ResponseEntity<UserInfoDto> getUserInfoByEmail(@PathVariable String email) {
        System.out.println("---------");
        UserInfoDto userInfoDto = userInfoService.getUserInfoByEmail(email);
        if (userInfoDto == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(userInfoDto, HttpStatus.OK);
    }
}
