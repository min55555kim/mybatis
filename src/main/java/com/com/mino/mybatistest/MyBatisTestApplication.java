package com.com.mino.mybatistest;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.com.mino.mybatistest.config")
public class MyBatisTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyBatisTestApplication.class, args);
	}

}
