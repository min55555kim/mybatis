package com.com.mino.mybatistest.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserInfoEntity {
    String email;
    String uuid;
    String reg_dt;
    String upd_dt;

    public String getEmail(){
        return this.email;
    }
    public String getUuid(){
        return this.uuid;
    }
    public String getReg_dt(){
        return this.reg_dt;
    }
    public String getUpd_dt() {
        return this.upd_dt;
    }
}
