package com.com.mino.mybatistest.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@NoArgsConstructor
public class UserInfoDto {
    String email;
    String uuid;
    String reg_dt;
    String upd_dt;

    public UserInfoDto(String email, String uuid, String reg_dt, String upd_dt) {
        this.email = email;
        this.uuid = uuid;
        this.reg_dt = reg_dt;
        this.upd_dt = upd_dt;
    }
}
