package com.com.mino.mybatistest.service.impl;

import com.com.mino.mybatistest.dto.UserInfoDto;
import com.com.mino.mybatistest.entity.UserInfoEntity;
import com.com.mino.mybatistest.mapper.UserInfoMapper;
import com.com.mino.mybatistest.service.UserInfoService;
import org.springframework.stereotype.Service;

@Service
public class UserInfoServiceImpl implements UserInfoService {

    private final UserInfoMapper userInfoMapper;

    public UserInfoServiceImpl(UserInfoMapper userInfoMapper) {
        this.userInfoMapper = userInfoMapper;
    }


    @Override
    public UserInfoDto getUserInfoByEmail(String email) {
        UserInfoEntity userInfoEntity = userInfoMapper.getUserInfoByEmail(email);
        // 필요에 따라 Entity를 DTO로 변환
        UserInfoDto userInfoDto = convertEntityToDto(userInfoEntity);
        return userInfoDto;
    }

    private UserInfoDto convertEntityToDto(UserInfoEntity userInfoEntity) {
        UserInfoDto userInfoDto = new UserInfoDto(
                userInfoEntity.getEmail(),
                userInfoEntity.getUuid(),
                userInfoEntity.getReg_dt(),
                userInfoEntity.getUpd_dt());
        return userInfoDto;
    }
}
