package com.com.mino.mybatistest.service;

import com.com.mino.mybatistest.dto.UserInfoDto;

public interface UserInfoService {
    UserInfoDto getUserInfoByEmail(String email);
}
